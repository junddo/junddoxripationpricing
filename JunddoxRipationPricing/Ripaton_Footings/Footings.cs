﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing
{
    public class Footing
    {
        public FootingType FootingType { get; set; }
        public FootingColor FootingColor { get; set; }

     public Footing(FootingType type, FootingColor color)
        {
            FootingType = type;
            FootingColor = color;
        }

        /// <summary>
        /// Return JUNDDO Price for one foot
        /// </summary>
        /// <returns></returns>
        public double GetPrice()
        {
            return Math.Round(PriceCalculation.FootingPrice.GetPrice(this), 2);
        }
          
    }
}
