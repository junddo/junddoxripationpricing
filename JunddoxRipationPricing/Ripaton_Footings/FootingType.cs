﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing
{
    /// <summary>
    /// Ripaton footing types
    /// </summary>
    public enum FootingType
    {
        Original_40cm,
        Original_71cm,
        Genereux_71cm,
        Cavalier_71cm,
    }

    public class FootWeightAttribute : Attribute
    {
        protected FootWeightAttribute(double weight)
        {
        }
    }

}
