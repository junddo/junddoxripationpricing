﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing.PriceCalculation
{
    /// <summary>
    /// Calculates Footing Price for one foot
    /// </summary>
    public static  class FootingPrice
    {
        /// <summary>
        /// Ripaton provided Customer Price for ONE foot, as per table provided on Apr 2018, TTC without discount
        /// </summary>
        /// <param name="footing">Ripaton Footing</param>
        /// <returns>price for this model, 0 if not defined</returns>
        public static double GetCustomerPrice(Footing footing)
        {
            switch (footing.FootingType)
            {
                case (FootingType.Original_71cm):
                    if (footing.FootingColor != FootingColor.Raw_Steel) return 30;
                    else return 0;
                case (FootingType.Original_40cm):
                    if (footing.FootingColor != FootingColor.Raw_Steel) return 23;
                    else return 0;
                case (FootingType.Genereux_71cm):
                    if (footing.FootingColor != FootingColor.Raw_Steel) return 40;
                    else return 30;
                case (FootingType.Cavalier_71cm):
                    if (footing.FootingColor != FootingColor.Raw_Steel) return 0;
                    else return 39;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Ripaton provided JUNDDO Price for ONE foot, as per table provided on Apr 2018, HT with discount
        /// </summary>
        /// <param name="footing">Ripaton Footing</param>
        /// <returns>price for this model, 0 if not defined</returns>
        public static double GetPrice(Footing footing)
        {
            double discount = 0.2;
            double TVARate = 1.2;

            return GetCustomerPrice(footing) * (1 - discount) / TVARate;
        }

        /// <summary>
        /// Ripaton provided JUNDDO Price for ONE foot, as per table provided on Apr 2018, HT with discount
        /// </summary>
        /// <param name="footing">Ripaton Footing</param>
        /// <returns>price for this model, 0 if not defined</returns>
        public static double GetPrice(Table table)
        {
            double discount = 0.2;
            double TVARate = 1.2;

            return GetCustomerPrice(table.Footing) * (1 - discount) / TVARate;
        }



        }
}
