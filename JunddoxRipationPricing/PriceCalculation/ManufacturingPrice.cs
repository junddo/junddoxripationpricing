﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing.PriceCalculation
{
    public static class ManufacturingPrice
    {
        /// <summary>
        /// Reference Price for a 120x650, bords arrondis
        /// </summary>
        const double referencePrice = 57.7;
        const double discountBordsDroits = 0.4; //facteur de reduction pour les bords droits

        public static double GetPrice(Table table)
        {
            double price = referencePrice;

            //Area factor, proportional
            double AreaFactor = (table.PanelArea / (1.2 * 0.65));
            price = price * (1+(AreaFactor-1)*0.3);

            //Add for extra Qunicaillerie
            price +=  7;

            //Bord factor
            switch (table.Bords)
            {
                case (Bords.Arrondis):  return price;
                case (Bords.Droits): return price * (1 - discountBordsDroits);
                default: return 0;
            }
        }


    }
}
