﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing.PriceCalculation
{
    public static class PanelPrice
    {

        const int extraLengthRounded = 50; 

        /// <summary>
        /// Panel price calculated with HBS 2018 prices
        /// </summary>
        public static double GetPrice(Table table)
        {
            Junddo.Woodprice.WoodPriceTable prices = Junddo.Woodprice.WoodPriceTable.HBS2018();
            if (table.Bords == Bords.Droits)
            {
                return prices.GetPrice(table.Wood, Table.PanelThickness, (uint)table.Length) * (table.Length * table.Width / (double)1000000)*(1+0.075);
            //    return prices.GetPriceLowQty(table.Wood, Table.PanelThickness, (uint)table.Length) * (table.Length * table.Width / (double)1000000);
            }
            else
            {
                return prices.GetPrice(table.Wood, Table.PanelThickness, ((uint)table.Length) + extraLengthRounded) * ((table.Length + extraLengthRounded) * (table.Width + extraLengthRounded) / (double)1000000) * (1 + 0.075); // we add an offset for CN machining
           //     return prices.GetPriceLowQty(table.Wood, Table.PanelThickness, ((uint)table.Length)+extraLengthRounded) * ((table.Length+extraLengthRounded) * (table.Width+extraLengthRounded) / (double)1000000); // we add an offset for CN machining
            }
        }

    }
}
