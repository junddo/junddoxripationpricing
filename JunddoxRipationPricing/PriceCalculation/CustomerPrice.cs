﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing.PriceCalculation
{
    public static class CustomerPrice
    {
        const double MarginRate = 0.38;

        /// <summary>
        /// Get HT Customer Price
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static double GetPrice(Table table)
        {
            double margin;
            return calculatePrice(table,out margin);
        }

        /// <summary>
        /// Get HT Margin
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static double GetMargin(Table table)
        {
            double margin;
            calculatePrice(table, out margin);
            return margin;
        }

        private static double calculatePrice(Table table, out double Margin)
        {
            Table tableCopy = new Table(table);

            //modification pour le prix de vente
            tableCopy.Bords = Bords.Arrondis;


            double PanelTotalPrice = tableCopy.PanelPrice + tableCopy.ManufacturingPrice + tableCopy.PackagePrice + tableCopy.GeodisTransportPrice + tableCopy.FootingPrice;
            double PanelMargin = PanelTotalPrice * MarginRate;

            //    Margin = PanelMargin + PanelTotalPrice - table.PanelPrice-table.ManufacturingPrice-table.PackagePrice-table.GeodisTransportPrice-table.FootingPrice;

            Margin = 0;

            return PanelMargin + PanelTotalPrice;
        }


    }
}
