﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JunddoRipatonPricing.PriceCalculation
{
    /// <summary>
    /// Compute transport price according to Geodis 2018 provided prices
    /// </summary>
    static class GeodisPrice
    {
        const double SurchargeCarburant = 0.15;

        /// <summary>
        /// Compute all included Geodis price, as an average on all French departments 
        /// </summary>
        /// <param name="weight">parcel weights</param>
        /// <returns>computed price</returns>
        public static double GetPrice(double weight)
        {
            double price = RawPrice(weight);
            price = price + 1; //surcharge sureté
            price = price + 2; //surcharge On Demand
            price = price * (1 + SurchargeCarburant);
            return price;
        }


        /// <summary>
        /// Raw prices provided by Geodis Apr 2018, for basic MES Service 'pied du camion'
        /// </summary>
        /// <param name="weight">weight of the packages sum</param>
        /// <returns></returns>
        private static double RawPrice(double weight)
        {
            if (weight <= 4) return 15.64;
            if (weight <= 9) return 18.91;
            if (weight <= 14) return 22.20;
            if (weight <= 29) return 28.76;
            if (weight <= 39) return 32.23;
            if (weight <= 49) return 35.71;
            if (weight <= 59) return 39.11;
            if (weight <= 69) return 42.51;
            if (weight <= 79) return 45.91;
            if (weight <= 89) return 49.31;
            if (weight <= 99) return 52.71;
            if (weight <= 199) return 52.71 * weight / (double)100;
            if (weight <= 299) return 50.07 * weight / (double)100;
            if (weight <= 499) return 47.57 * weight / (double)100;
            if (weight <= 1000) return 45.19 * weight / (double)100;

            return 0;
        }
    }


    }
