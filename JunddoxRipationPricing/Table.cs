﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Junddo.Woodprice;

namespace JunddoRipatonPricing
{
    public class Table
    {
        internal const int PanelThickness = 28;

        public int Length { get; set; }

        public int Width { get; set; }

        public WoodSpecy Wood { get; set; }

        public Footing Footing { get; set; }

        public Bords Bords { get; set; }

        public Table() { }

        public Table(int length, int width, WoodSpecy wood, Bords bords)
        {
            Length = length;
            Width = width;
            Wood = wood;
            Bords = bords;
        }


        public Table(int length, int width, WoodSpecy wood, Bords bords, FootingType footingType, FootingColor footingColor)
        {
            Length = length;
            Width = width;
            Wood = wood;
            Bords = bords;
            Footing = new Footing(footingType, footingColor);
        }

        public Table(Table other)
        {
            foreach (System.Reflection.PropertyInfo prop in typeof(Table).GetProperties())
            {
               if(prop.CanWrite) prop.SetValue(this, prop.GetValue(other));
            }
        }


        /// <summary>
        /// Weight of the panel in kg
        /// </summary>
        public int PanelWeight { get { return ComputeWeight(); } }

        public double PanelVolume { get { return ComputeVolume(); } }

        public double PanelArea { get { return ComputeArea(); } }

        public double PanelPrice { get { return Math.Round(PriceCalculation.PanelPrice.GetPrice(this), 2) + (double)5; } } //add a shipping price for the panel

        public double ManufacturingPrice { get { return Math.Round(PriceCalculation.ManufacturingPrice.GetPrice(this), 2); } }

        public double PackagePrice { get { return (double)10; } }          

        /// <summary>
        /// Price for the 4 feet of the table
        /// </summary>
        public double FootingPrice
        {
            get
            {
                if (this.Footing == null) return 0;
                return 4 * Math.Round(PriceCalculation.FootingPrice.GetPrice(this), 2);
            }
        }

        public double GeodisTransportPrice { get { return Math.Round(PriceCalculation.GeodisPrice.GetPrice(this.PanelWeight+10), 2); } }

        public double CustomerHTPrice { get { return Math.Round(PriceCalculation.CustomerPrice.GetPrice(this), 2); } }

        public double CustomerTTCPrice { get { return 1.2*Math.Round(PriceCalculation.CustomerPrice.GetPrice(this), 2); } }

       // public double Margin { get { return 1.2 * Math.Round(PriceCalculation.CustomerPrice.GetMargin(this), 2); } }

        private int ComputeWeight()
        {
            double Volume = Length * Width * PanelThickness / (double)Math.Pow(10, 9);
            return (int)(Volume * 850);
        }

        private double ComputeVolume()
        {
            double Volume = Length * Width * PanelThickness / (double)Math.Pow(10, 9);
            return Volume;
        }

        private double ComputeArea()
        {
            double Area = Length * Width / (double)Math.Pow(10, 6);
            return Area;
        }

        /// <summary>
        /// Generate a list of table for analysis
        /// </summary>
        /// <returns></returns>
        public static List<Table> GenerateTableList()
        {
            List<Table> tableList = new List<Table>();
            WoodSpecy[] woodOptions = { WoodSpecy.Chene_A, WoodSpecy.Chene_Rustique };
            Bords[] bordptions = { Bords.Arrondis, Bords.Droits };

            for (int length = 700; length <= 2400; length += 100)
            {
                for (int width = 500; width <= Math.Min(1100, length); width += 100)
                {
                    foreach (WoodSpecy wood in woodOptions)
                    {
                        foreach (Bords bords in bordptions)
                        {
                            Table table = new Table(length, width, wood, bords);
                            table.Footing = new Footing(FootingType.Original_71cm, FootingColor.Black_Mat);
                            tableList.Add(table);
                        }
                    }
                }
            }
            return tableList;
        }

        /// <summary>
        /// Serialize a list of Tables into a CSV File
        /// </summary>
        /// <param name="list"></param>
        /// <param name="csvPath"></param>
        public static void SerializeCSV(List<Table> list, string csvPath)
        {
            System.IO.TextWriter textWriter = new System.IO.StreamWriter(csvPath);
            CsvHelper.CsvWriter csv = new CsvHelper.CsvWriter(textWriter);
            csv.WriteRecords<Table>(list);
            textWriter.Close();
            textWriter.Dispose();

        }
    }
}
